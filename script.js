import { TextureLoader } from 'https://cdn.jsdelivr.net/npm/three@0.121.1/build/three.module.js';

// Variables globales
let scene, camera, renderer, cube;
const cubeWidth = 2;
const cubeHeight = 2;
const cubeDepth = 2;
let keys = {};
let score = 0;
let timeLeft = 20;
let isWinDisplayed = false;
let isLoseDisplayed = false;
let gems = [];
let gem;

const restartButton = document.getElementById('restart-button');

const gemSize = 1;
const gemTexture = new TextureLoader().load('img/texture-emerald.jpg', () => {
    // Met à jour les matériaux une fois que la texture est chargée
    gemMaterial.map = gemTexture;
    gemMaterial.needsUpdate = true;
});
 
const gemMaterial = new THREE.MeshPhongMaterial({ color: 0x00ff00});

function updateTime() {
    if(timeLeft > 0){
        timeLeft--;
    }
    document.getElementById('time').innerText = `Time: ${timeLeft}`;
}

setInterval(updateTime, 1000);  

restartButton.addEventListener('click', function() {
    location.reload();
});

function addGems() {
    const gemGeometry = new THREE.DodecahedronGeometry(gemSize, 0);
    for (let i = 0; i < 10; i++) {
      const gem = new THREE.Mesh(gemGeometry, gemMaterial);
      gem.isGem = true;
      gem.name="gem";
      gem.castShadow = true;
      gem.position.x = Math.random() * 16 - 8;
      gem.position.z = Math.random() * 16 - 8;
      gem.position.y = gemSize / 2;
      scene.add(gem);
      gems.push(gem);
    }
}

function isColliding(object) { // Définition de la fonction avec un paramètre "object" qui est l'objet à tester
    const distanceX = Math.abs(cube.position.x - object.position.x); // Calcul de la distance horizontale entre le bord du cube et l'objet
    const distanceZ = Math.abs(cube.position.z - object.position.z); // Calcul de la distance verticale entre le bord du cube et l'objet
    const halfSizeX = cubeWidth / 2 + object.geometry.parameters.width / 2; // Calcul de la demi-largeur du cube et de l'objet selon l'axe x
    const halfSizeZ = cubeDepth / 2 + object.geometry.parameters.depth / 2; // Calcul de la demi-largeur du cube et de l'objet selon l'axe z
    const halfSizeEmeraldX = cubeWidth / 2 + object.scale.x * gemSize / 2; // Calcul de la demi-largeur de l'émeraude (si l'objet est une émeraude)
    const halfSizeEmeraldZ = cubeDepth / 2 + object.scale.z * gemSize / 2; // Calcul de la demi-hauteur de l'émeraude (si l'objet est une émeraude)

    //Si on prends les largeurs entières, les objets sont detectés avant même que le cube touche l'objet alors il est nécessaire de calculer avec les demi-largeurs
    if (distanceX <= halfSizeX && distanceZ <= halfSizeZ) { 
      return true;
    }
    if (object.name === "gem") { 
      if (distanceX <= halfSizeEmeraldX && distanceZ <= halfSizeEmeraldZ) { 
        return true; 
      }
    }
  
    return false; 
}

function moveCube() {
    if (cube && cube.position) {
        const currentPosition = cube.position.clone();
        if (keys['z']) {
            cube.position.z -= 0.2;
        }
        if (keys['q']) {
            cube.position.x -= 0.2;
        }
        if (keys['s']) {
            cube.position.z += 0.2;
        }
        if (keys['d']) {
            cube.position.x += 0.2;
        }
         // Vérifier la collision avec chaque mur
        scene.children.forEach((child) => {
            if (child instanceof THREE.Mesh && child !== cube && !child.isGem && isColliding(child)) {
                if (isColliding(child)) {
                    // Annuler le mouvement en cas de collision
                    cube.position.copy(currentPosition);
                }
            }
        });
        // Vérifier la collision avec chaque gemme
        gems.forEach((gem) => {
        if (isColliding(gem)) {
          // Supprimer la gemme et augmenter le score
          scene.remove(gem);
          gems = gems.filter((g) => g !== gem);
          score += 1;
          document.getElementById('score').innerText = `Score : ${score}`;
        }
        });
        if (score === 10 && cube.position.x > 15 && !isWinDisplayed && timeLeft !== 0) {
            alert('WIN !');
            isWinDisplayed = true;
            restartButton.style.display = 'block';
            cube.position.set(2, 1, 0);
        }
        if (timeLeft === 0 && !isLoseDisplayed && !isWinDisplayed) {
            alert('Game over!');
            isLoseDisplayed = true;
        }
    }
}


// Fonction d'initialisation
function init() {
    // Créer une scène
    scene = new THREE.Scene();

    restartButton.style.display = 'none';
    // Créer une caméra
    
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.set(0, 20, 5);
    camera.rotation.set(250, 0, 0);
    // Ajouter un sol
    const floorTexture = new TextureLoader().load('img/sol.jpg', () => {
        // Mettez à jour les matériaux une fois que la texture est chargée
        floorMaterial.map = floorTexture;
        floorMaterial.needsUpdate = true;
    });
    const floorGeometry = new THREE.PlaneGeometry(20, 20);
    const floorMaterial = new THREE.MeshPhongMaterial({ color: 0x808080, side: THREE.DoubleSide});
    const floor = new THREE.Mesh(floorGeometry, floorMaterial);
    floor.position.set(0, 0, 0);
    floor.rotation.x = -Math.PI / 2;
    floor.receiveShadow = true;
    scene.add(floor);

    const wallTexture = new TextureLoader().load('img/laby.jpg', () => {
        // Mettez à jour les matériaux une fois que la texture est chargée
        wallMaterial.map = wallTexture;
        wallMaterial.needsUpdate = true;
    });
    // Ajouter des murs
    const wallHeight = 5;
    const wallThickness = 0.5;
    const wallMaterial = new THREE.MeshPhongMaterial({ color: 0x888888});

    //Mur 1
    const wall1Geometry = new THREE.BoxGeometry(20, wallHeight, wallThickness);
    const wall1 = new THREE.Mesh(wall1Geometry, wallMaterial);
    wall1.position.set(0, wallHeight / 2, -10);
    wall1.castShadow = true;
    wall1.receiveShadow = true;
    scene.add(wall1);

    //Mur 2
    const wall2Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 20);
    const wall2 = new THREE.Mesh(wall2Geometry, wallMaterial);
    wall2.position.set(-10, wallHeight / 2, 0);
    wall2.castShadow = true;
    wall2.receiveShadow = true;
    scene.add(wall2);

    //Mur 3
    const wall3Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 16);
    const wall3 = new THREE.Mesh(wall3Geometry, wallMaterial);
    wall3.position.set(10, wallHeight / 2, 2);
    wall3.castShadow = true;
    wall3.receiveShadow = true;
    scene.add(wall3);

    //Mur 4
    const wall4Geometry = new THREE.BoxGeometry(20, wallHeight, wallThickness);
    const wall4 = new THREE.Mesh(wall4Geometry, wallMaterial);
    wall4.position.set(0, wallHeight / 2, 10);
    wall4.castShadow = true;
    wall4.receiveShadow = true;
    scene.add(wall4);

    //Mur 5
    const wall5Geometry = new THREE.BoxGeometry(10, wallHeight, wallThickness);
    const wall5 = new THREE.Mesh(wall5Geometry, wallMaterial);
    wall5.position.set(5, wallHeight / 2, -6);
    wall5.castShadow = true;
    wall5.receiveShadow = true;
    scene.add(wall5);

    //Mur 6
    const wall6Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 8);
    const wall6 = new THREE.Mesh(wall6Geometry, wallMaterial);
    wall6.position.set(0, wallHeight / 2, -2);
    wall6.castShadow = true;
    wall6.receiveShadow = true;
    scene.add(wall6);

    // Mur 7
    const wall7Geometry = new THREE.BoxGeometry(6.5, wallHeight, wallThickness);
    const wall7 = new THREE.Mesh(wall7Geometry, wallMaterial);
    wall7.position.set(0, wallHeight / 2, 2);
    wall7.castShadow = true;
    wall7.receiveShadow = true;
    scene.add(wall7);

    // Mur 8
    const wall8Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 4);
    const wall8 = new THREE.Mesh(wall8Geometry, wallMaterial);
    wall8.position.set(3.5, wallHeight / 2, 0);
    wall8.castShadow = true;
    wall8.receiveShadow = true;
    scene.add(wall8);

    // Mur 9
    const wall9Geometry = new THREE.BoxGeometry(3, wallHeight, wallThickness);
    const wall9 = new THREE.Mesh(wall9Geometry, wallMaterial);
    wall9.position.set(8.5, wallHeight / 2, -2);
    wall9.castShadow = true;
    wall9.receiveShadow = true;
    scene.add(wall9);

    // Mur 10
    const wall10Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 8);
    const wall10 = new THREE.Mesh(wall10Geometry, wallMaterial);
    wall10.position.set(7, wallHeight / 2, 2);
    wall10.castShadow = true;
    wall10.receiveShadow = true;
    scene.add(wall10);

    // Mur 11
    const wall11Geometry = new THREE.BoxGeometry(7, wallHeight, wallThickness);
    const wall11 = new THREE.Mesh(wall11Geometry, wallMaterial);
    wall11.position.set(0.5, wallHeight / 2, 6);
    wall11.castShadow = true;
    wall11.receiveShadow = true;
    scene.add(wall11);

    // Mur 12
    const wall12Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 8);
    const wall12 = new THREE.Mesh(wall12Geometry, wallMaterial);
    wall12.position.set(-6.5, wallHeight / 2, -6);
    wall12.castShadow = true;
    wall12.receiveShadow = true;
    scene.add(wall12);

    //Mur 13
    const wall13Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 4);
    const wall13 = new THREE.Mesh(wall13Geometry, wallMaterial);
    wall13.position.set(-3, wallHeight / 2, 8);
    wall13.castShadow = true;
    wall13.receiveShadow = true;
    scene.add(wall13);

    //Mur 14
    const wall14Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 6);
    const wall14 = new THREE.Mesh(wall14Geometry, wallMaterial);
    wall14.position.set(-3, wallHeight / 2, -1);
    wall14.castShadow = true;
    wall14.receiveShadow = true;
    scene.add(wall14);

    //Mur 15
    const wall15Geometry = new THREE.BoxGeometry(3.5, wallHeight, wallThickness);
    const wall15 = new THREE.Mesh(wall15Geometry, wallMaterial);
    wall15.position.set(-8, wallHeight / 2, 2);
    wall15.castShadow = true;
    wall15.receiveShadow = true;
    scene.add(wall15);

    //Mur 16
    const wall16Geometry = new THREE.BoxGeometry(wallThickness, wallHeight, 5);
    const wall16 = new THREE.Mesh(wall16Geometry, wallMaterial);
    wall16.position.set(-6, wallHeight / 2, 4.5);
    wall16.castShadow = true;
    wall16.receiveShadow = true;
    scene.add(wall16);

    const cubeTexture = new TextureLoader().load('img/cube.jpg', () => {
        // Mettez à jour les matériaux une fois que la texture est chargée
        cubeMaterial.map = cubeTexture;
        cubeMaterial.needsUpdate = true;
    });
    // Ajouter un cube
    const cubeGeometry = new THREE.BoxGeometry(cubeWidth, cubeHeight, cubeDepth);
    const cubeMaterial = new THREE.MeshPhongMaterial({ color: 0xff0000});
    cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
    cube.position.set(2, 1, 0);
    cube.castShadow = true;
    cube.receiveShadow = true;
    scene.add(cube);
    

    // un rendu WebGL
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    document.body.appendChild(renderer.domElement);

    // Ajouter une lumière ambiante
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
    scene.add(ambientLight);

    // Ajouter une lumière directionnelle
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    directionalLight.position.set(10, 30, 0);
    directionalLight.castShadow = true;
    directionalLight.shadow.camera.near = 0.1;
    directionalLight.shadow.camera.far = 50;
    directionalLight.shadow.camera.left = -10;
    directionalLight.shadow.camera.right = 10;
    directionalLight.shadow.camera.top = 10;
    directionalLight.shadow.camera.bottom = -10;
    directionalLight.shadow.mapSize.width = 2048;
    directionalLight.shadow.mapSize.height = 2048;
    scene.add(directionalLight);
    // Gestion des événements clavier
    document.addEventListener('keydown', (event) => {
        keys[event.key] = true;
    });
    document.addEventListener('keyup', (event) => {
        keys[event.key] = false;
    });
    // Gestion des événements
    window.addEventListener('resize', onWindowResize);
}

// Fonction pour gérer le redimensionnement de la fenêtre
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

// Boucle d'animation
function animate() {
    
    requestAnimationFrame(animate);
    moveCube();
    if (cube && camera) {
        camera.position.x = cube.position.x;
        camera.position.y = cube.position.y + 20;
        camera.position.z = cube.position.z;
        camera.lookAt(cube.position);
    }
    renderer.render(scene, camera);
}

// Initialiser et lancer l'animation


init();
addGems();
animate();

