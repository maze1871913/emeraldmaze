# EmeraldMaze

## Objectif du jeu : Le joueur doit traverser un labyrinthe en collectant toutes des émeraudes et s'en échapper dans un temps imparti.

## Version 1 : 
> Créez un labyrinthe en 3D
## Version 2 : 
> Ajoutez un personnage que le joueur peut contrôler à l'aide des touches du clavier.
## Version 3 : 

> Générer 10 émeraudes à des emplacements aléatoire dans le labyrinthe que l'on peut récupérer.
> Chaque émeraude récupérer obtroie 1 point au joueur. 
> Ajout d'un temps imparti pour récupérer les émeraudes et sortir du labyrinthe.
> Ajout de l'affichage "WIN" lorsque les conditions sont remplie.
